import server from '../src/index'
import request from 'supertest'

describe('Tests the server root paths', () => {
    test('Root path should return a object with OK message', async () => {
        const { statusCode, body } = await request(server).get('/')
        expect(statusCode).toBe(200)
        expect(body).toHaveProperty('message')
        expect(body.message).toBe('OK')
    })
})