import express, { Request, Response } from 'express'
import serverless from 'serverless-http'
import morgan from 'morgan'

const app = express()
app.use(morgan('short'))

app.get('/', (_req: Request, res: Response) => {
    res.send({ message: 'OK' })
})

app.get('/healthcheck', (_req: Request, res: Response) => {
    res.send({ health: 'checked' })
})

export const handler = serverless(app)
export default app