#!/bin/bash
WEBHOOK_URL="https://discord.com/api/webhooks/843563132496773152/nKc0qD6803qgid1ktTM63lh0_CthKZwYpKpX3ZGsfdTzoRJaJ0CjRq-wObOfqpIUl2Jj"
STAGE="$1"
if [ -z $STAGE ]; then 
    echo "Provide a stage" && exit 1
fi

npx serverless deploy --stage $STAGE | tee .sls-output
DEPLOYMENT_URL=$(
    cat .sls-output | \
    grep ANY - | \
    sed \
        -e 's/ANY - //g' \
        -e 's/\/{proxy+}//g' \
        -e 's/^ *//g' \
        -e 's/ *$//g'
)
TITLE="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-'Deploy'}"
URL="${DEPLOYMENT_URL:-'https://google.com'}"
curl -X POST $WEBHOOK_URL \
-H'Content-type: application/json' --data-binary @- << EOF
{
    "username": "",
    "avatar_url": "",
    "content": "Release novo do **Cati Server Sample** fresquinho!",
    "embeds": [
        {
            "title": "$TITLE",
            "color": 571865,
            "timestamp": "",
            "url": "$URL"
        }
    ]
}
EOF
rm .sls-output